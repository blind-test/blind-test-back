﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestApiRest.Model;

namespace TestApiRest.Services
{
    public class TestServices
    {

        private readonly GroupContext _context;

        public TestServices(GroupContext context)
        {
            _context = context;
        }



        public bool AttributeScore(Guid idGroup, bool hasGoodTitle, bool hasGoolSinger, bool hasGoodBonus)
        {

            Group myGroup = _context.Groups.FirstOrDefault(x => x.Id == idGroup);
            if (myGroup == null)
            {
                return false;
            }
            else
            {
                if (hasGoodTitle)
                {
                    myGroup.Score++; 
                }
                if (hasGoolSinger)
                {
                    myGroup.Score++;
                }
                if (hasGoodBonus)
                {
                    myGroup.Score++;
                }
                _context.SaveChanges();
                return true;
            }
        }


    
    }
}
