﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using TestApiRest.Model;
using TestApiRest.filter;
using System;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TestApiRest.Controllers
{
    [Route("api/[Controller]")]
    [Produces("application/json")]
    public class GroupController : Controller
    {
        private readonly GroupRepository _groupRepository;


        public GroupController(GroupRepository groupRepository)
        {

            this._groupRepository = groupRepository;            
        }

        //private readonly GroupContext _context;



        // GET api/groups/id
        [HttpGet("{id}", Name = "GetGroup")]
        public IActionResult GetById(Guid id)
        {
            var item = _groupRepository.FindGroup(id);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        // GET api/group
        [HttpGet]
        public IEnumerable<Group> GetAllGroup()
        {

            return _groupRepository.FindAll().OrderByDescending(x=> x.Score);
        }

        [HttpPost]
        public IActionResult Create([FromBody] Group item)
        {
            if (item == null)
            {
                return BadRequest();
            }

            _groupRepository.Insert(item);

            return CreatedAtRoute("GetGroup", new { id = item.Id }, item);
        }



        [HttpPut("{id}")]
        public IActionResult Update(Guid id, [FromBody] Score score)
        {
            if (score.addScore.Equals(0) )
            {
                return BadRequest();
            }

            var todo = _groupRepository.FindGroup(id);
            if (todo == null)
            {
                return NotFound();
            }
            Group updateGroup = new Group()
            {
                Id = todo.Id,
                Name = todo.Name,
                Score = todo.Score + score.addScore
            };
               
            _groupRepository.Update(updateGroup);

            return  CreatedAtRoute("GetGroup", new { id = updateGroup.Id }, updateGroup); ;
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            var mygroup = _groupRepository.FindGroup(id);
            if (mygroup == null)
            {
                return NotFound();
            }

            _groupRepository.Delete(mygroup);
            return new NoContentResult();
        }


    }
}
