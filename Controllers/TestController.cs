﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using TestApiRest.Model;
using TestApiRest.filter;
using System;
using TestApiRest.websockets;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace TestApiRest.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class TestController : Controller
    {
  
        private readonly GroupRepository _groupRepository;

        private readonly BuzzHandler _buzzHandler;

        private JsonSerializerSettings _settings;


        public TestController(GroupRepository context, BuzzHandler buzzHandler)
        {
            _groupRepository = context;
            _buzzHandler = buzzHandler;
            _settings = new JsonSerializerSettings();
            _settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
        }

        //GetAllBuzz
        [HttpGet(Name ="GetAllBuzz")]
        public IEnumerable<Group> GetAllBuzz()
        {
            List<Group> groupsAnswers = new List<Group>();

            if (BuzzCollection.Instance.AnswersList.Any())
            {
                foreach (Buzz item in BuzzCollection.Instance.AnswersList)
                {
                    groupsAnswers.Add(_groupRepository.FindGroup(item.Id));
                }
                return groupsAnswers;
            }
            else
                return null;
        }


        // POST buzz
        [HttpPost]
        public IActionResult Create([FromBody] Buzz buzz)
        {
            if (BuzzCollection.Instance.AddNewChallenger(buzz.Id))
            {
                Group group = _groupRepository.FindGroup(buzz.Id);
                _buzzHandler.SendMessageToAllAsync(Newtonsoft.Json.JsonConvert.SerializeObject(group, Newtonsoft.Json.Formatting.None, _settings));
            }            
            //return CreatedAtRoute("GetAllBuzz", null);
            return new NoContentResult();
        }

        //// POST buzzer/id
        //[HttpPost]
        //public IActionResult Create([FromBody]int id)
        //{
        //    BuzzCollection.Instance.AddNewChallenger(id);
        //    return CreatedAtRoute("GetAllBuzz", BuzzCollection.Instance.AnswersList.First(x => x.Id == id));
        //}


        [HttpDelete()]
        public IActionResult Delete()
        {
            BuzzCollection.Instance.InitiaizeAnswersList();
            return new NoContentResult();
        }

        [HttpPut("{id}")]
        public IActionResult Update(Guid id, [FromBody] Buzz item)
        {
            BuzzCollection.Instance.AnswersList.First(x=> x.Id == id).CanAnswer= item.CanAnswer;
            return new NoContentResult();
        }


    }
}
