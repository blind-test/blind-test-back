﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TestApiRest.Model
{
    public class GroupRepository
    {
        
        private IMongoCollection<Group> _collection;

        public GroupRepository()
        {
            var client = new MongoDB.Driver.MongoClient("mongodb://localhost:27017");
            var database = client.GetDatabase("blind-test");
            _collection = database.GetCollection<Group>("groups");
        }

        public void Insert(Group group)
        {
            _collection.InsertOne(group);
        }

        public Group FindGroup(Guid id)
        {
            return _collection.Find(Builders<Group>.Filter.Eq("_id", id)).FirstOrDefault();
        }

        public IEnumerable<Group> FindAll()
        {
            return _collection.Find(Builders<Group>.Filter.Empty).ToEnumerable();
        }

        public void Update(Group group)
        {

            FilterDefinition<Group> filter = Builders<Group>.Filter.Eq("_id", group.Id);
            UpdateDefinition<Group> update = Builders<Group>.Update.Set("Score", group.Score);

            _collection.UpdateOne(filter, update); 
        }

        public void Delete(Group group)
        {
            FilterDefinition<Group> filter = Builders<Group>.Filter.Eq("_id", group.Id);
            _collection.DeleteOne(filter);
        }



        


        


    

    }
}
