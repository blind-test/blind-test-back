﻿using System;

namespace TestApiRest.Model
{
    public class Buzz
    {
        public Guid Id { get; set; }
        public bool CanAnswer { get; set; }
    }
}
