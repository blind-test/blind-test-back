﻿using Microsoft.EntityFrameworkCore;


namespace TestApiRest.Model
{
    public class GroupContext : DbContext
    {
        public GroupContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Group> Groups { get; set; }
    }
}
