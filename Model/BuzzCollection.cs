﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestApiRest.Model
{
    public sealed class BuzzCollection
    {


        public static readonly Lazy<BuzzCollection> instance = new Lazy<BuzzCollection>(() => new BuzzCollection());

        public static BuzzCollection Instance { get { return instance.Value; } }

        public BuzzCollection() 
        {
    
            InitiaizeAnswersList();
        }
     


        public List<Buzz> AnswersList;
  

        public void InitiaizeAnswersList()
        {
            AnswersList = new List<Buzz>();
 
        }

        public  bool AddNewChallenger(Guid id)
        {
            if (!AnswersList.Any(x => x.Id == id))
            {
                AnswersList.Add(new Buzz() { Id = id, CanAnswer = true });
                return true;
            }
            return false;
        }

        public Guid GetFirstCanAnswer()
        {
            return AnswersList.FirstOrDefault(x => x.CanAnswer).Id; 

        }

      

    }
}
