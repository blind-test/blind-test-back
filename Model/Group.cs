﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;
using System;

namespace TestApiRest.Model
{
    public class Group
    {

        [BsonId(IdGenerator = typeof(CombGuidGenerator))]
        public Guid Id { get; set; }

        public string Name { get; set; }

        public int Score { get; set; }
     
    }
}
